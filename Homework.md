Name: Roberto Bertolini
ID: 111635119
Email: roberto.bertolini@stonybrook.edu
BBucketID: roberto_bertolini

I am a first year graduate student in the Applied Mathematics and Statistics program at Stony Brook University.
I completed my undergraduate degree in pure mathematics at the University of Rochester. I am from New York City and grew 
up in the Ozone Park neighborhood of Queens. 

I am in the Computational Applied Mathematics (CAM) track. My research interests are in optimization and machine learning, 
focusing predominately on the predictive modeling of stochastic processes. I am also interested in statistics 
and operations research. 

Outside of school, I enjoy karate, swimming, and reading. I love 90's rock music and enjoy traveling.

Thank you Nicolas for showing me around campus and telling me about your graduate experience when I came to visit in March. 
I am looking forward to my time at Stony Brook.

I change something¡£