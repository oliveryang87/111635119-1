%BERTOLINI_MATLAB_Problem_2.m
%   This program uses the Monte Carlo approach for approximating "pi/4" 
%   using the relative areas of a square and an inscribed circle. The
%   program plots the ratio of the points inside a circle of radius 1 and 
%   points that lie outside the circle, which are randomly generated. The
%   user specifies the number of significant digits and the program rounds
%   the result off to a number of digits specified.
%
% INPUTS: 
%       n: number of points
%       prec: user level of precision between 2 and 2^29
%
% OUTPUTS: 
%       ratio: computed value of pi from the simulation
%       prec_str: estimated value of pi using significant digits
%       
% Other m files required: None
% Subfunctions: None
            
%AUTHOR: Roberto Bertolini
%ID: 111635119
%Email: roberto.bertolini@stonybrook.edu
%Date: October 20, 2017

%%%%%%%%%%%%%%%%%%%%%%%%%%%BEGIN MAIN FUNCTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Bertolini_MATLAB_Problem_2()

% User inputs
n=input('Number of points: ');
prec = input('Enter number of significant digits: ');

% Generate random points and center the circle at the point (.5,.5) 
x=rand(n,1);
y=rand(n,1);
x1=x;
y1=y; %circle has centre at (0.5,0.5)
r=x1.^2+y1.^2;
inside=0;   %Number of points inside circle

% Plot specifications (color and shape)
figure('color','white');
hold on
axis square;

for i=1:n
    if r(i)<=0.25
        % If the value of r is less than or equal to .25, the point lies in
        % the circle so color it blue, and add one to the count tally
        plot(x(i),y(i),'b.');
        pause(.1)
        title(sprintf('Mathematical Estimation of Pi    N = %i', i));
        inside=inside+1;
    else
        % If the plot is outside the circle, color the point red, and do
        % not update the count tally
        plot(x(i),y(i),'r.');
        pause(.01)
        title(sprintf('Mathematical Estimation of Pi    N = %i', i));
    end
end

% Computed value of pi without significant digits
ratio = inside/(0.25*n);
fprintf('Actual Estimate: %s \n' ,ratio);

% Computes estimated value of pi using significant digits

ratio_result= num2str(ratio,prec);
ratio_result= str2num(ratio_result);

%ratio_result = round(ratio,prec,'significant');
fprintf('Precision Estimate: %s \n',ratio_result);

% Plots the precision result on the graph
text(1,1,'Precision')
text(1,.95,num2str(ratio_result,prec))
hold off;

end 

%%%%%%%%%%%%%%%%%%%%%%%%%%%END MAIN FUNCTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%