%BERTOLINI_MATLAB_Problem_4.m
%   This program implements a two-dimensional version of cellular automata,
%   a grid representation of cells that can be used to model physical and
%   biological processes. The program generates a random matrix of cells
%   that are called 'alive' if their value is one and dead if their value
%   is zero. We apply the following rule:

%   1) 'A cell will die in the next generation if more than four of its neighbors 
%   are dead at the present time.' 

%   2) 'A cell will come back to life or stay alive in the next generation if
%   four or more of its neighbors are alive'

%   This changes the representation of our plot at different time steps

% INPUTS: 
%       ntimes_run - the number of times we want the program to run. The
%       default number is 1000. 
%
% OUTPUTS: 
%       matrix - the current configuration of the graph at time step 1000.
%       Zeros indicate that the cell is dead and one indicates that the
%       cell is alive

% Other m files required: None
% Subfunctions:
        %1) Append
            % Input: matrix (current configuration state)
            %   Based on the current configuration state, this function
            %   appends a square of zeros around the current matrix. This
            %   will used to tally the number of adjacent neighbors for a
            %   particular cell
            % Output: matrix_app (the augmented matrix)
        %2) Neighborhoods
            % Input: matrix_app, n (size of universe given as input by user)
            %   Calculates the number of adjacent alive cells in a Moore
            %   neighborhood around each cell in matrix_app
            % Output: alive (matrix tallying the number of live cells in
            %         matrix_app
        %3) Update
            % Input: alive (matrix tallying the number of live cells for a
            %        given time step
            %           Updates the matrix from the previous time step with
            %           updated live and dead cells
            % Output: matrix2 (contains updated matrix with live and dead
            %         cells at the current time step)
            
%AUTHOR: Roberto Bertolini
%ID: 111635119
%Email: roberto.bertolini@stonybrook.edu
%Date: October 16, 2017

%%%%%%%%%%%%%%%%%%%%%%%%%%%BEGIN MAIN FUNCTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [matrix] = Bertolini_MATLAB_Problem_4(ntimes)

if nargin<1; ntimes = 1000; end

% User input for size of universe
n = input('Size of universe: ');

% Create inital grid with randomized entries. 
matrix = randi([0 1],n,n);

% Plots the initial configuration. Note that the spy plot only shows
% entries which have a one in them

figure(1)
spy(matrix)
title(' Cellular Automata Grid N = 1')
pause(1)

% For loop to plot successive generations
count = 2;
while count~=ntimes+1
    matrix_app = Append(matrix);
    alive = Neighborhoods(matrix_app,n);
    matrix = Update(alive);
    figure(1)
    spy(matrix)
    title(sprintf(' Cellular Automata Grid N = %i', count));
    pause(1)
    count = count +1;
end 
end 

%%%%%%%%%%%%%%%%%%%%%%%%%%%END MAIN FUNCTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Adds a square of zeros to our matrix for the purposes of tallying the
% number of live and dead cells

function matrix_app = Append(matrix)
matrix_zeros = cat(2, zeros(size(matrix,1),1), matrix ,zeros(size(matrix,1),1));
matrix_app = [zeros(1,size(matrix_zeros,2));matrix_zeros;zeros(1,size(matrix_zeros,2))];
end 

% Calculates the number of alive neighbors around a eight neighbor radius
% for a given cell (positions (i ? 1, j ? 1), (i ? 1, j), (i ? 1, j + 1), 
% (i, j ? 1), (i, j + 1), (i + 1, j ? 1), (i + 1, j) and (i + 1, j + 1)).
% This value is reflected in the i,j entry of the matrix 'alive'

function alive = Neighborhoods(matrix_app,n)
alive = zeros(n);
m = size(matrix_app);
for i=2:m-1
    for j=2:m-1
        if (matrix_app(i-1,j-1) == 1)
            alive(i-1,j-1) = alive(i-1,j-1)+1;
        end 
        if (matrix_app(i-1,j) == 1)
            alive(i-1,j-1) = alive(i-1,j-1)+1;
        end
        if (matrix_app(i-1,j+1) == 1)
            alive(i-1,j-1) = alive(i-1,j-1)+1;
        end 
        if (matrix_app(i,j-1) == 1)
            alive(i-1,j-1) = alive(i-1,j-1)+1;
        end 
        if (matrix_app(i,j+1) == 1)
            alive(i-1,j-1) = alive(i-1,j-1)+1;
        end 
        if (matrix_app(i+1,j-1) == 1)
            alive(i-1,j-1) = alive(i-1,j-1)+1;
        end
        if (matrix_app(i+1,j) == 1)
            alive(i-1,j-1) = alive(i-1,j-1)+1;
        end 
        if (matrix_app(i+1,j+1) == 1)
            alive(i-1,j-1) = alive(i-1,j-1)+1;
        end 
    end 
end 
end 

% Applies the rule defined in the heading to the cells:

%   1) 'A cell will die in the next generation if more than four of its neighbors 
%   are dead at the present time.' 

%   2) 'A cell will come back to life or stay alive in the next generation if
%   four or more of its neighbors are alive'

function matrix2 = Update(alive)
for i=1:size(alive,1)
    for j=1:size(alive,2)
        if alive(i,j)<=4
            matrix2(i,j) = 0;
        else
            matrix2(i,j) = 1;
        end
    end 
end 
end 