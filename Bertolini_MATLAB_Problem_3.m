%BERTOLINI_MATLAB_Problem_3.m
%   This program computes the product of a row vector, matrix, and column
%   vector, and utilizes exception handling to catch incorrect input from
%   the user. There are no inputs needed for the program to run.
%
% INPUTS: 
%       vec_u_sz: the length of the row vector
%       mat_A_sz: the dimensions of the matrix A. We then extract the
%       number of rows and number of columns for the matrix and put them
%       into variables entitled "mat_A_row" and "mat_A_col"
%       vec_v_sz: the length of the column vector
%
% OUTPUTS: 
%       result - returns the resulting product which is a 1 x 1 matrix

% Other m files required: None
% Subfunctions:
        %1) Product
            % Input: vec_u,mat_A,vec_v
            %   Calculates the matrix product by looping through the rows
            %   of the vectors and matrices when calculating their inner
            %   product
            % Output: returns the resulting product which is a 1 x 1 matrix
            
%AUTHOR: Roberto Bertolini
%ID: 111635119
%Email: roberto.bertolini@stonybrook.edu
%Date: October 16, 2017

%%%%%%%%%%%%%%%%%%%%%%%%%%%BEGIN MAIN FUNCTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function result = Bertolini_MATLAB_Problem_3()

% Introduction to the program
disp('Welcome to the Inner Product Program! It computes:')
disp('u_tranpose * A* v where u, v are vectors and A is a matrix')

% Input using the prompt command allows the user to input the vector and
% matrix sizes in an external window

% Size of vector u

prompt = {'Enter the size of vector u: '};
dlg_title = 'Input';
num_lines = 1;
defaultans = {''};
vec_u_sz = inputdlg(prompt,dlg_title,num_lines,defaultans);
vec_u_sz = str2num(vec_u_sz{1});

% Dimensions of matrix A

prompt = {'Number of Rows of A: ', 'Number of Columns of A : '};
num_lines = 1;
defaultans = {'',''};
mat_A_sz = inputdlg(prompt,dlg_title,num_lines,defaultans);
mat_A_row = str2num(mat_A_sz{1});
mat_A_col = str2num(mat_A_sz{2});

% Size of vector v

prompt = {'Enter the size of vector v: '};
num_lines = 1;
defaultans = {''};
vec_v_sz = inputdlg(prompt,dlg_title,num_lines,defaultans);
vec_v_sz = str2num(vec_v_sz{1});

% The following exceptions make sure that the user inputs data that is not 
% a character, the user does not leave input blank, and the number is positive. 
% If one of these exceptions is activated, the program quits and returns 
% the resulting message.

assert(size(vec_u_sz,1) > 0, 'Vector size must be a positive integer!!');
assert(size(mat_A_row,1) > 0, 'Matrix row size must be a positive integer!!');
assert(size(mat_A_col,1) > 0, 'Matrix column size must be a positive integer!!');
assert(size(vec_u_sz,1) > 0, 'Vector size must be a positive integer!!');

% Assign random entries to matrix 
vec_u = rand(vec_u_sz,1);
mat_A = rand(mat_A_row,mat_A_col);
vec_v = rand(vec_v_sz,1);

% These exceptions make sure that the dimensions of matrix multiplication 
% agree so that the inner product can be performed. No errors occur if the
% size of the vector u is equal to the number of rows of A and the number
% of columns of A are equal to the size of the vector v

assert(size(vec_u,1)== mat_A_row, 'Vector U and Matrix A are incompatible for vector-matrix multiplication.');
assert(mat_A_col==size(vec_v,1), 'Matrix A and Vector V are incompatible for matrix-vector multiplication.');

% Computes the resulting product

result = Product(vec_u',mat_A,vec_v);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%END MAIN FUNCTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculates the matrix product by looping through the rows
% of the vectors and matrices when calculating their inner
% product

function answer = Product(vec_u,mat_A,vec_v)
y = zeros(size(vec_u,1),size(mat_A,2));
% Computes the product u*A
for i=1:size(mat_A,1)
    y = y + vec_u(i)*mat_A(i,:);
end 
answer = zeros(size(y,1),size(vec_v,2));
% Computes the resulting product by v and returns the result
for i=1:size(vec_v,1)
    answer = answer + y(i)*vec_v(i,:);
end 

end 